<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Titulo</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
        .card {
            margin-top: 10px;
        }
    </style>
</head>

<body style="background-color: #ffe;">
    <!-- content -->
    <div class="container ">
        <?php if (is_array($results)) : ?>


            <div class="row">
                <div class="card border-success col-sm ">
                    <div class="card-body">
                        <div class="row">
                            <p>Olá, meu nome é <?= $nome; ?>, </p>
                            <p>Abaixo, segue histórico do rastreio de sua encomenda.</p>
                            <p>Você pode fazer essa consulta diretamente no link https://status-correios.herokuapp.com/.</p>
                            <p>E ainda enviar por e-mail para si mesmo ou para um amigo.</p>                           
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="card border-success col ">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h3 class="center">
                                    Útimo Status do Objeto: <b><?= $codigo; ?></b>
                                </h3>
                                <table class="table table-striped table-sm">

                                    <tbody>
                                        <tr>
                                            <td>Status: <b><?= $results[0]['action']; ?></b></td>
                                        </tr>
                                        <tr>
                                            <td>Data: <b><?= $results[0]['date']; ?></b> | Hora: <b><?= $results[0]['hour']; ?></b></td>
                                        </tr>
                                        <tr>
                                            <td>Local: <b><?= $results[0]['location']; ?></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="card border-success col-sm ">
                    <div class="card-body">
                        <h3 class="center"><b>Histórico do Objeto</b></h3>
                        <h5 class="center">Detalhes sobre o trajeto do objeto: <?= $codigo; ?></h5>


                        <?php if (isset($results) && !empty($results) && is_array($results)) : ?>
                            <?php foreach ($results as $result) : ?>
                                <hr>
                                <table class="table-striped table-sm col-sm">
                                    <tbody>
                                        <tr>
                                            <td>Status: <b><?= $result['action']; ?></b></td>
                                        </tr>
                                        <tr>
                                            <td>Data: <?= $result['date']; ?>
                                                | Hora: <?= $result['hour']; ?>
                                                | <?= $result['change']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Local: <?= $result['location']; ?></td>
                                        </tr>

                                    </tbody>

                                </table>

                            <?php endforeach; ?>

                        <?php else : ?>



                        <?php endif; ?>




                    </div>
                </div>



            </div>
            <div class="row">
                <div class="card border-success col-sm ">
                    <div class="card-body">
                        <div class="row">
                            <p>Atenciosamente,</p><br>
                            <p><b><?= $nome; ?>.</b></p>                    
                        </div>
                    </div>
                </div>

            </div>
        <?php else : ?>
            <div class="row">
                <div class="card border-success col-sm ">
                    <div class="card-body">
                        <h3 class="center"><b><?= $results; ?></b></h3>

                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
   
    <!-- ./content -->


</body>

</html>