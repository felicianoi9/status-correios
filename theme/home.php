<div class="container ">
    <?php if (!empty($message)):?>
        <div class="row">
            <div class="alert alert-success col-sm" role="alert">
                <?= $message;?>
            </div>

        </div>
    <?php endif;?>
    <div class="row">
        <div class="card border-secondary col-sm ">
            <div class="card-body">
                <form class="" action="/resultado" method="POST">
                    <div class="form-row">
                        <div class="col-md-9 mb-3">
                            <input type="text" class="form-control" name="codigo" placeholder="Digite o código de rastreio">

                        </div>
                        <div class="col-md-3 mb-3">
                            <button type="submit" class="form-control text-white bg-success">Buscar</button>

                        </div>
                    </div>
                    <div class="form-row">

                    </div>
                </form>
            </div>
        </div>

    </div>
   
</div>