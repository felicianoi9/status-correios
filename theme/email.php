<div class="container ">
    <div class="row">
        <div class="card border-success col-sm ">
            <div class="card-body">
                <h3 class="center">Útimo Status do Objeto: <b><?= $codigo; ?></b></h3>
                <table class="table table-striped table-sm">

                    <tbody>
                        <tr>
                            <td>Status: <b><?= $results[0]['action']; ?></b></td>
                        </tr>
                        <tr>
                            <td>Data: <b><?= $results[0]['date']; ?></b> | Hora: <b><?= $results[0]['hour']; ?></b></td>
                        </tr>
                        <tr>
                            <td>Local: <b><?= $results[0]['location']; ?></b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card border-success col-sm ">
            <div class="card-body">
                <h3 class="center"><b>Histórico do Objeto</b></h3>
                <h5 class="center">Detalhes sobre o trajeto do objeto: <?= $codigo; ?></h5>
                

                    <?php if (isset($results) && !empty($results)) : ?>
                        <?php foreach ($results as $result) : ?>
                        <hr>
                        <table class="table-striped table-sm col-sm">
                            <tbody>
                                <tr>
                                    <td>Status: <b><?= $result['action']; ?></b></td>
                                </tr>
                                <tr>
                                    <td>Data: <?= $result['date']; ?> 
                                    | Hora: <?= $result['hour']; ?>
                                    | <?= $result['change']; ?></td>
                                </tr>
                                <tr>
                                    <td>Local: <?= $result['location']; ?></td>
                                </tr>
                               
                            </tbody>

                        </table>
                        
                        <?php endforeach;?> 

                    <?php else : ?>
                    <?php endif; ?>

                


            </div>
        </div>



    </div>
</div>

