<div class="container ">
    <?php if (is_array($results)) : ?>
        <div class="row">
            <div class="card border-secondary col-sm ">
                <div class="card-body">
                    <form class="" action="/email" method="POST">
                        <div class="form-row">
                            <div class="col-md-5 mb-3">
                                <input type="text" class="form-control" name="nome" placeholder="Digite o nome">

                            </div>
                            <div class="col-md-5 mb-3">
                                <input type="email" class="form-control" name="email" placeholder="Digite o e-mail">

                            </div>
                            <input type="hidden" name="codigo" value="<?= $codigo; ?>">
                            <div class="col-md-2 mb-3">
                                <button type="submit" class="form-control text-white bg-success">Enviar por E-mail</button>

                            </div>
                        </div>
                        <div class="form-row">

                        </div>
                    </form>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="card border-success col-sm ">
                <div class="card-body">
                    <div class="row">
                        <div class="col"><img width="100%" src="../storage/img/progressbar/entrega.gif" alt=""></div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <?= $status[0]['status']; ?>
                        </div>
                        <div class="col">
                            <?= $status[1]['status']; ?>
                        </div>
                        <div class="col">
                            <?= $status[2]['status']; ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="card border-success col ">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h3 class="center">
                                Útimo Status do Objeto: <b><?= $codigo; ?></b>
                            </h3>
                            <table class="table table-striped table-sm">

                                <tbody>
                                    <tr>
                                        <td>Status: <b><?= $results[0]['action']; ?></b></td>
                                    </tr>
                                    <tr>
                                        <td>Data: <b><?= $results[0]['date']; ?></b> | Hora: <b><?= $results[0]['hour']; ?></b></td>
                                    </tr>
                                    <tr>
                                        <td>Local: <b><?= $results[0]['location']; ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="card border-success col-sm ">
                <div class="card-body">
                    <h3 class="center"><b>Histórico do Objeto</b></h3>
                    <h5 class="center">Detalhes sobre o trajeto do objeto: <?= $codigo; ?></h5>


                    <?php if (isset($results) && !empty($results) && is_array($results)) : ?>
                        <?php foreach ($results as $result) : ?>
                            <hr>
                            <table class="table-striped table-sm col-sm">
                                <tbody>
                                    <tr>
                                        <td>Status: <b><?= $result['action']; ?></b></td>
                                    </tr>
                                    <tr>
                                        <td>Data: <?= $result['date']; ?>
                                            | Hora: <?= $result['hour']; ?>
                                            | <?= $result['change']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Local: <?= $result['location']; ?></td>
                                    </tr>

                                </tbody>

                            </table>

                        <?php endforeach; ?>

                    <?php else : ?>



                    <?php endif; ?>




                </div>
            </div>



        </div>
    <?php else : ?>
        <div class="row">
            <div class="card border-success col-sm ">
                <div class="card-body">
                    <h3 class="center"><b><?= $results; ?></b></h3>

                </div>
            </div>
        </div>
    <?php endif; ?>
</div>