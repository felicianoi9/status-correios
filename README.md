# Projeto - Status de rastreadores dos Correios
O objetivo desse exercício é construir uma aplicação em PHP, que permita receber
status de rastreadores dos Correios e que encaminhe os resultados por email para um
cliente.

## Check
Formatar esses status de entrega em um email, e disparar utilizando o PHPMailer
(enviar para joao.macedo@elastic.fit). No corpo do email você pode colocar dados
fictícios nos locais dos dados do cliente, mas é legal colocar seu nome pra ajudar
na identificação;

## Check
Criar e anexar um PDF contendo das mesmas informações que contém no email.

## Comentários sobre o desenvolvimento do exercício:
### Quais tecnologias usou
Para esse projeto foram utilizados, PHP 7 Orientado a Objeto, Composer, MVC (inspirado no Laravel), pacotes de gestão de rotas (coffeecode/router), de gestão de envio de e-mail (PHPMailer), de criação de PDF (Dompdf), GitLab, HTML, CSS, Bootstrap, Crawler para condumir status de rastreio (adaptação de https://github.com/luannsr12/correios-rastreio),  

### Quais as maiores dificuldades
A primeira foi econtrar uma api de rastreio que gostasse e achasse adequada. Por isso, adaptei um projeto encontrado no GitHub (https://github.com/luannsr12/correios-rastreio), pois assim, consumiria dados diretamente dos correios. Essa adaptação consumiu mais tempo que o previsto, acarretando pouca atenção para o layout do e-mail e PDF que ficaram bem básicos.

### O que não conseguiu fazer e o motivo
Não consegui fazer uma layout adequado com css para o e-mail e o pdf, pois a adaptação do Crawler da api dos correios tomou tempo além do esperado. Também não consegui tempo para fazer os dois últimos ítens da lista exta de atividades.

## Itens extras (não obrigatório)
### Check
Identificar o status da entrega como "Postado", "Encaminhado", "Erro" ou
"Entregue" de acordo com a tabela de status dos Correios e utilizar isso para
modificar o assunto e o título do email;

### No Check
Fazer essa aplicação de uma forma que permita verificar o status de centenas de
encomendas, ou seja, a aplicação recebe um objeto com vários códigos de
rastreio;

### No Check
No PDF e no email (um dos dois ou ambos), inserir o status com a forma de uma
barra de progresso (no email com css, e no PDF com gráficos).

## Deploy
Vai encontrar uma versão do projeto no link https://status-correios.herokuapp.com/ que espero estar funcionando após enviar este projeto, 

## To do List
#### Usar AWS como servidor de email;
#### Melhorar o css do corpo do email e do pdf;
#### Verificar centenas de statatus;
#### Inserir status com forma de barra de progresso no email e no pdf.