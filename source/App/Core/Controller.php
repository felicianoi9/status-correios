<?php 

namespace Source\App\Core;

class Controller 
{
    public function loadView($viewName, $viewData = [])
    {
        extract($viewData);
        require __DIR__.'/../../../theme/'.$viewName.'.php';
    }
    public function loadTemplate($viewName, $viewData = [])
    {        
        extract($viewData);
        require __DIR__.'/../../../theme/template.php';
    }
    public function loadViewInTemplate($viewName, $viewData = [])
    {
        extract($viewData);
        require __DIR__.'/../../../theme/'.$viewName.'.php';
    }
}