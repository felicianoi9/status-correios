<?php

namespace Source\App\Support;

use Dompdf\Dompdf;
use Source\App\Core\Controller;

class PDFHelper extends Controller
{
    public function create($data):void
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        
        ob_start();
        
        $this->loadView('pdf_body', $data);
        $dompdf->loadHtml(ob_get_clean());
  
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();
        $codigo = $data['codigo'];

        // Output the generated PDF to Browser        
        file_put_contents("./storage/pdf/".$codigo.".pdf", $dompdf->output());
    }
}
