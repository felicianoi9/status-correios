<?php

namespace Source\App\Support;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Dotenv\Dotenv;

class Mail 
{
    public function __construct()
    {
        $dotenv = Dotenv::createImmutable(__DIR__."/../../../");
        $dotenv->load();
    }
    public function send($nome, $email, $result, $subject, $codigo)
    {        
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);


        try {
            //Server settings
            $mail->SMTPDebug = 0;                      
            $mail->isSMTP();                                            
            $mail->Host       = $_ENV['MAIL_HOST'];                    
            $mail->SMTPAuth   = true;                                  
            $mail->Username   = $_ENV['MAIL_USER'];                    
            $mail->Password   =  $_ENV['MAIL_PASSWORD'];                               
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         
            $mail->Port       = $_ENV['MAIL_PORT']; 
            $mail->CharSet = "utf-8";                               

            //Recipients
            $mail->setFrom($_ENV['MAIL_FROM_EMAIL'], $_ENV['MAIL_FROM_NAME']);
            $mail->addAddress($email, $nome);             
            $mail->addCC($_ENV['MAIL_FROM_EMAIL_ACC']);

            // Content
            $mail->isHTML(true);
            $mail->setLanguage('br');                            
            $mail->Subject = $subject;
            $mail->Body    = $result;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->addAttachment(__DIR__."/../../../storage/pdf/".$codigo.".pdf");

            $resolve = $mail->send();
            return $resolve;
            exit;
          
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }


       
    }
}
