<?php 

namespace Source\App\Controllers;

use Source\App\Core\Controller;

class OpsController extends Controller
{
    public function index($ops = [])
    {
        $data = [
            'ops' =>$ops,
        ];
        $this->loadTemplate('ops', $data);
    }
}