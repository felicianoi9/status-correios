<?php

namespace Source\App\Controllers;

use Source\App\Core\Controller;
use Source\App\Services\Track;

class HomeController extends Controller
{
    public function index($message="")
    {     
        $data = [
            'message' => $message,                
        ];     

        $this->loadTemplate('home', $data);
    }
}