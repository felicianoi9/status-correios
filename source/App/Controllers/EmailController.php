<?php 

namespace Source\App\Controllers;

use Source\App\Core\Controller;
use Source\App\Services\Track;
use Source\App\Support\Mail;
use CoffeeCode\Router\Router;
use Source\App\Support\PDFHelper;


class EmailController extends Controller
{
    protected $router;
    
    public function __construct()
    {
        $this->router = new Router("https://status-correios.herokuapp.com");
        
        
    }
    public function index($post)
    {
        if (isset($post) && !empty($post)) {
            $track = new Track();  
            $codigo = $post['codigo'];   
            $tracked = $track->getTrack($post['codigo']);
            $result = $tracked['historico'];
            $status = $tracked['status'];   
            $nome = explode(" ", $post['nome'])[0]; 
            $data = [
                'codigo' => $codigo,
                'results' => $result,
                'status' => $status,
                'nome' => $nome,
            ];

        } else {
            $data = [
                'url' => $_ENV['URL_BASE'],
            ];
        }

        $email = trim($post['email']);

        if ($email=="joao.macedo@elastic.fit") {
            $email_body = 'email_body_macedo';
            
        } else {
            $email_body = 'email_body';
        }
       
        ob_start();
        $this->loadView($email_body, $data);       
        $html= ob_get_clean();     
        
        /**
         * Criando pdf
         */
        $pdf = new PDFHelper();
        $pdf->create($data); //retorna o path do pdf
        
        $mail = new Mail();
        
        

        $subject = "Status do rastreio de sua encomenda: ".$status[2]['status'];

        $response = $mail->send($nome, $email, utf8_decode($html), $subject, $codigo);
        if ($response) {
            $message = "sucesso";
        } else {
            $message = "erro";
        }

        // $this->router->redirect("/{$message}");
        $this->router->redirect("/");
        exit;
        
    }
    public function body($post){
        if (isset($post) && !empty($post)) {
            $nome = explode(" ", $post['nome'])[0];
            $track = new Track();  
            $codigo = $post['codigo'];   
            $tracked = $track->getTrack($post['codigo']);
            $result = $tracked['historico'];
            $status = $tracked['status'];    
            $data = [
                'url' => URL_BASE,
                'codigo' => $codigo,
                'results' => $result,
                'status' => $status,
                'nome' => $nome
            ];

        } else {
            $data = [
                'url' => URL_BASE,
            ];
        }
        $this->loadView('email_body', $data);
    }
}