<?php 

namespace Source\App\Controllers;

use Source\App\Core\Controller;
use Source\App\Services\Track;
use CoffeeCode\Router\Router;

class ResultController extends Controller
{
    protected $router;
    
    public function __construct()
    {
        $this->router = new Router(URL_BASE);
    }
    public function index($post = [])
    {
        if (isset($post) && !empty($post)) {
            $track = new Track();     
            $tracked = $track->getTrack($post['codigo']);
            $result = $tracked['historico'];
            $status = $tracked['status'];
            $data = [
                'url' => URL_BASE,
                'codigo' => $post['codigo'],
                'results' => $result,
                'status' => $status,
            ];

        } else {
            $data = [
                'url' => URL_BASE,
            ];
        }   
        
        $this->loadTemplate('results', $data);

    }
}