<?php

namespace Source\App\Services;

use CoffeeCode\Router\Router;

class Track
{
    
    public function getTrack($obj)
    {
        if ($obj) {
            $post = array('Objetos' => $obj);

            /**
             * Status do topo
             */
           
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://www2.correios.com.br/sistemas/rastreamento/resultado.cfm");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            $output = curl_exec($ch);
            curl_close($ch);
            /**
             * Tratamento do Status de topo
             */
            $status = [
                "0" =>["status"=>"", "data"=>""],
                "1" =>["status"=>"", "data"=>""],
                "2" =>["status"=>"", "data"=>""],
            ];           
            // implementação do $status : "0" =>["status"=>"", "data"=>""]
            $topo = explode("<div id=\"EventoPostagem\" style=\"float:left;width:33%\">", $output)[1];
            $topo = explode("<strong>", $topo);
            $topo[3] = explode("<script type=\"text/javascript\" async=\"\" src=\"https://www.google-analytics.com/analytics.js\">", $topo[3])[0];
            $temp = explode("<strong>", $topo[1])[0];
            $temp = str_replace("<text>","",$temp);
            $temp = str_replace("</text>","",$temp);
            $temp = explode("\"", $temp)[0];
            $temp = str_replace("</strong>","",$temp);
            $temp = explode("<br />", $temp);
            $temp[1] = str_replace("</div>","",$temp[1]);
            $temp[1] = trim(str_replace("<div id=","",$temp[1]));
            $status[0]['status'] = utf8_encode(trim($temp[0]));
            $status[0]['data'] = utf8_encode(trim($temp[1]));

            // implementação do $status : "1" =>["status"=>"", "data"=>""]
            $temp = explode("<div id=\"DataEntrega\" style=\"float:right;width:33%\">", $topo[2])[0];
            $temp = str_replace("</strong>","",$temp);
            $temp = explode("<br />", $temp);
            $temp[1] = str_replace("</div>","",$temp[1]);
            $temp[1] = trim(str_replace("<div id=","",$temp[1]));
            $status[1]['status'] = utf8_encode(trim($temp[0]));
            $status[1]['data'] = utf8_encode(trim($temp[1]));

            // implementação do $status : "2" =>["status"=>"", "data"=>""]
            $temp = explode("<strong>", $topo[3])[0];
            $temp = str_replace("</strong>","",$temp);
            $temp = explode("<br />", $temp);
            $temp[1] = str_replace("</div>","",$temp[1]);
            $temp[1] = str_replace("</div>","",$temp[1]);
            $temp[1] = str_replace("</div>","",$temp[1]);
            $status[2]['status'] = utf8_encode(trim($temp[0]));
            $status[2]['data'] = utf8_encode(trim($temp[1]));
            
            /**
             * Tratamento do Histórico
             */
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://www2.correios.com.br/sistemas/rastreamento/resultado_semcontent.cfm");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            $output = curl_exec($ch);
            curl_close($ch);
            $out = explode("table class=\"listEvent sro\">", $output);
            

            if (isset($out[1])) {
                $output = explode("<table class=\"listEvent sro\">",$output);
                $output = explode("</table>",$output[1]);
                $output = str_replace("</td>","",$output[0]);
                $output = str_replace("</tr>","",$output);
                $output = str_replace("<strong>","",$output);
                $output = str_replace("</strong>","",$output);
                $output = str_replace("<tbody>","",$output);
                $output = str_replace("</tbody>","",$output);
                $output = str_replace("<label style=\"text-transform:capitalize;\">","",$output);
                $output = str_replace("</label>","",$output);
                $output = str_replace("&nbsp;","",$output);
                $output = str_replace("<td class=\"sroDtEvent\" valign=\"top\">","",$output);
                $output = explode("<tr>",$output);
                $novo_array = array();
                foreach($output as $texto){
                    $info   = explode("<td class=\"sroLbEvent\">",$texto);
                    $dados  = explode("<br />",$info[0]);
                    $dia   = trim($dados[0]);
                    $hora  = trim(@$dados[1]);
                    $local = trim(@$dados[2]);
                    $dados = explode("<br />",@$info[1]);
                    $acao  = trim($dados[0]);
                    $exAction   = explode($acao."<br />",@$info[1]);
                    $acrionMsg  = strip_tags(trim(preg_replace('/\s\s+/', ' ',$exAction[0])));
                    if(""!=$dia){
                        $exploDate = explode('/',$dia);
                        $dia1 = $exploDate[2].'-'.$exploDate[1].'-'.$exploDate[0];
                        $dia2 = date('Y-m-d');
                        $diferenca = strtotime($dia2) - strtotime($dia1);
                        $dias = floor($diferenca / (60 * 60 * 24));
                        $change = utf8_encode("há {$dias} dias");
                        $novo_array[] = array("date"=>$dia,"hour"=>$hora,"location"=>$local,"action"=>utf8_encode($acao),"message"=>utf8_encode($acrionMsg),"change"=>utf8_decode($change));
                    }
                }                
                return ["status" => $status, "historico" => $novo_array];
            } else {
                return "Objeto não encontrado";
            }
        } else {
            return "Parametro vazio";
        }
    }
}
