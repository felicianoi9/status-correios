<?php


use CoffeeCode\Router\Router;

$router = new Router(URL_BASE);

/**
 * Controllers
 */
$router->namespace("Source\App\Controllers");

/**
 * HomeController
 */
$router->group(null);
$router->get("/{message}", "HomeController:index");
$router->get("/", "HomeController:index");
$router->post("/", "HomeController:index");



/**
 * ResultController
 */
$router->group("resultado");
$router->post("/", "ResultController:index");

/**
 * EmailController
 */
$router->group("email");
$router->post("/", "EmailController:index");
$router->post("/body", "EmailController:body");

/**
 * Track
 */

$router->group("track");
$router->post("/", "Web:track");

/**
 * Pdf's 
 */
$router->group("pdfs");
$router->get("/", "PdfController:index");
$router->post("/", "PdfController:teste");

/**
 * Errors 
 */
$router->group("oooops");
$router->get("/{errorcode}", "OpsController:index");



$router->dispatch();

if ($router->error()) {
    
    $router->redirect("/oooops/{$router->error()}");
}